const {ObjectID} = require("mongodb");

const {mongoose} = require('./../server/db/mongoose');
const {Todo} = require("./../server/models/todo");
const {User} = require("./../server/models/user");

//Todo.remove()

//remove ALL (pass empty object)

// Todo.remove({}).then((result) => {
//   console.log(result);
// });

// Todo.findOneAndRemove
//Todo.findByIdAndRemove

Todo.findByIdAndRemove('586d16348d6e1b01709f487a').then((todo) => {
  console.log(todo);
});
