const {ObjectID} = require("mongodb");

const {mongoose} = require('./../server/db/mongoose');
const {Todo} = require("./../server/models/todo");
const {User} = require("./../server/models/user");

var id = '586a202b600aab04cdd99cab';

if(!ObjectID.isValid(id)){
  console.log('ID not Valid');
  return;
}

// Todo.find({
//   _id: id
// }).then((todos) => {
//   console.log('Todos', todos);
// });
//
// Todo.findOne({
//   _id: id
// }).then((todo) => {
//   if(!todo){
//     console.log('Id not found');
//   }
//   console.log('Todo', todo);
// });
//
// Todo.findById(id).then((todo) => {
//   if(!todo){
//     console.log('Id not found');
//   }
//   console.log('Todo', todo);
// }).catch((err) => console.log(err));

User.findById(id).then((user)=>{
  if(!user){
    console.log('Id not found');
  }
  console.log('User', user);
}).catch((err) => console.log(err));
