// const MongoClient = require("mongodb").MongoClient;
const {MongoClient, ObjectID} = require("mongodb"); // object descructuring ES6

// var obj = new ObjectID();
// console.log(obj);

MongoClient.connect('mongodb://localhost:27017/TodoApp', (err, db) =>{
  if(err){
    console.log('unable to connect to mongodb server');
    return;
  }
  console.log('connected to mongodb server');

  // get ALL documents from Todos collection
  db.collection('Todos').find().toArray().then((docs) => {
    console.log('Todos');
    console.log(JSON.stringify(docs, undefined, 2));
  }, () => {
    console.log('Unable to fetch todos', err);
  });

  // // get documents that has completed value true
  // db.collection('Todos').find({completed: true}).toArray().then((docs) => {
  //   console.log('Todos that are completed');
  //   console.log(JSON.stringify(docs, undefined, 2));
  // }, () => {
  //   console.log('Unable to fetch todos', err);
  // });
  //
  // //get document based on document id
  // db.collection('Todos').find({
  //   _id: new ObjectID('5864d6db29244139c0f80bf3')
  // }).toArray().then((docs) => {
  //   console.log('Todos that are completed');
  //   console.log(JSON.stringify(docs, undefined, 2));
  // }, () => {
  //   console.log('Unable to fetch todos', err);
  // });
  //
  // //get number of documents
  // db.collection('Todos').find().count().then((count) => {
  //   console.log(`Todos count: ${count}`);
  // }, () => {
  //   console.log('Unable to fetch count of todos', err);
  // });

  //delete a document Many
  // db.collection('Users').deleteMany({name: 'Andrew'}).then((result) => {
  //   console.log(result);
  // });

  //delete one
  // db.collection('Todos').deleteOne({text: 'Eat lunch'}).then((result) => {
  //   console.log(result);
  // });

  //findOneAndDelete
  // db.collection('Users').findOneAndDelete({_id: new ObjectID('5864e81729244139c0f80bf8')}).then((result) => {
  //   console.log(JSON.stringify(result, undefined, 2));
  // });

  //findOneAndUpdate
  // db.collection('Todos').findOneAndUpdate({
  //   _id: new ObjectID('5864db0129244139c0f80bf6')
  // },{
  //   $set: {
  //     completed: true
  //   }
  // },{
  //   returnOriginal: false
  // }).then((result) => {
  //   console.log(JSON.stringify(result, undefined, 2));
  // });

  db.collection('Users').findOneAndUpdate({
    _id: new ObjectID('5864d473407da9073714fc1e')
  },{
    $set: {
      name: 'Naveen'
    },
    $inc: {
      age: 3
    }
  },{
    returnOriginal: false
  }).then((result) => {
    console.log(JSON.stringify(result, undefined, 2));
  });

  // db.close();
});
